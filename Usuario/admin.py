from django.contrib import admin
from .models import Usuario, Mascota

admin.site.register(Usuario)
admin.site.register(Mascota)