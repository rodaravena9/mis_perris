from django import forms

from .models import Usuario, Mascota

class RegForm(forms.ModelForm):

    class Meta:
        model = Usuario
        fields = ('correo', 'rut', 'nombre', 'fechaN','telefono', 'region', 'ciudad', 'tipoCasa')


class MascForm(forms.ModelForm):
    fotografia = forms.ImageField()
    class Meta:
        model = Mascota
        fields = (
            'fotografia',
            'nombre',
            'razaPredominante',
            'descripcion',
            'estado',
        )