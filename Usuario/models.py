from django.db import models

# Create your models here.
class Usuario(models.Model):
    correo = models.CharField(max_length=40)
    rut = models.CharField(max_length=11)
    nombre = models.CharField(max_length=40)
    fechaN = models.DateField()
    telefono = models.IntegerField()
    region =  models.CharField(max_length=40)
    ciudad =  models.CharField(max_length=40)
    tipoCasa =  models.CharField(max_length=40)

    def guardar(self):
        self.save()
    
    def __str__(self):
        return self.nombre

class Mascota(models.Model):
    fotografia = models.ImageField(upload_to='fotos/')
    nombre = models.CharField(max_length=40)
    razaPredominante = models.CharField(max_length=40)
    descripcion = models.TextField()
    estado = models.CharField(max_length=20)


    def guardar(self):
        self.save()
    
    def __str__(self):
        return self.nombre